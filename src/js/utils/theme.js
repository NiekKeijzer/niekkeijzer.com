const switchName = 'light-switch';


export function registerTheme() {
  const lightSwitch = document.getElementById(switchName);

  lightSwitch.addEventListener('change', (event) => {
    if (event.target.checked) {
      localStorage.setItem(switchName, true);
    } else {
      localStorage.removeItem(switchName);
    }
  });
}


export function loadTheme() {
  const lightSwitch = document.getElementById(switchName);
  const lightsOn = localStorage.getItem(switchName);

  lightSwitch.checked = lightsOn;
}


export default { registerTheme, loadTheme };
