/**
 * main.js
 * Entrypoint for webpack
 */
import ready from './utils/ready';
import registerServiceWorker from './utils/serviceWorker';
import { info } from './utils/debug';
import { registerTheme, loadTheme } from './utils/theme';

function onReady(e) {
  loadTheme();
  registerTheme();

  registerServiceWorker();
  info(`Event: ${e.type}`, `Datestamp: ${this.date}`);
}

ready(onReady, {
  date: new Date(),
});
