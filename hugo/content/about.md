---
layout: single
title: About
---
I am a geeky travel aficionado with a passion for technology. In my
 spare time, when I am not playing around with new software, you are
 likely to find me in the kitchen. More often than not cooking up a
 mediterranean dish, sometimes with mixed results.

Even though I consider myself predominantly a backend developer, I am
 more than aware that every project deserves a _incredible_ frontend. In
 my work I try to accommodate this by working together with the frontend
 developer(s) to create well thought out (REST) APIs that meets their
 needs with just the right amount of _magic_.

To create this, my language of choice is [Python](https://www.python.org).
 I often work with frameworks like [Tornado](http://tornadoweb.org) and
 [Sanic](https://github.com/huge-success/sanic) for their speed and
 ability to handle concurrent requests without resorting to threads.
 However, I do thoroughly enjoy the simplicity of
  [Django](https://www.djangoproject.com) and will often create a proof
  of concept using it.

Curious what we can do for each other? Hit me up on
 [hello@niekkeijzer.com](mailto:hello@niekkeijzer.com) or find me on one
 of the social media platforms listed below.
