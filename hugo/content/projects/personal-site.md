---
title: Personal site
date: 2019-04-10T07:40:08.964Z
demoUrl: 'https://niekkeijzer.com'
sourceUrl: 'https://gitlab.com/NiekKeijzer/niekkeijzer.com'
tags:
  - hugo
  - html
  - css
---
Almost everyone benefits from having an online presence. However, I feel
 that most current sites feel bloated, especially for a backend
 developer. That is why I created this site using a static site generator
 called [Hugo](https://www.gohugo.io)

The goal of this project is to first and foremost to give those
 interested some insight into what I do. Secondly I wanted a _fast_ site
 where content has the highest priority.
